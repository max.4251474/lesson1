package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Post struct {
	ID      int
	Author  string
	Picture string
	Text    string
	Comment []*Comment
}

type Comment struct {
	ID     int
	PostId int
	Author string
	Text   string
}

var lastPostId = 1
var postRepo = map[int]*Post{
	0: {
		ID:      0,
		Author:  "Максим",
		Picture: "https://user-images.githubusercontent.com/57855981/195186742-7be6059b-33e3-4903-b48b-cc5dc89649ac.png",
		Text:    "Мой кот!",
	},
}

func posts(w http.ResponseWriter, r *http.Request) {
	posts := make([]*Post, 0, len(postRepo))
	for _, post := range postRepo {
		posts = append(posts, post)
	}

	b, _ := json.Marshal(posts)
	w.Write(b)
}

func createPost(w http.ResponseWriter, r *http.Request) {
	post := Post{}
	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	post.ID = lastPostId
	postRepo[lastPostId] = &post
	b, _ := json.Marshal(lastPostId)
	lastPostId++

	w.Write(b)
}

func delPost(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello")
}

func createComment(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello")
}
func main() {
	http.HandleFunc("/posts", posts)
	http.HandleFunc("/create_post", createPost)
	http.HandleFunc("/del_post", delPost)
	http.HandleFunc("/create_comment", createComment)

	http.ListenAndServe("localhost:8080", nil)
}
